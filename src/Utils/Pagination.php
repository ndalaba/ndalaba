<?php
/**
 * Created by PhpStorm.
 * User: ndalaba
 * Date: 18/01/2018
 * Time: 14:51
 */

namespace App\Utils;

class Pagination {

    public const PER_PAGE=2;

    public function getPagination($route, $page = 1, $total, $route_params = []) {
        $pagination = array(
            'page' => $page,
            'route' => $route,
            'current_item_start' => ($page - 1) * self::PER_PAGE,
            'pages_count' => ceil($total / self::PER_PAGE),
            'route_params' => $route_params
        );
        return $pagination;
    }
}