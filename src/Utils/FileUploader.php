<?php
/**
 * Created by PhpStorm.
 * User: ndalaba
 * Date: 26/04/2017
 * Time: 13:27
 */

namespace App\Utils;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class FileUploader{

    private $upload_folder,$upload_path, $extensions, $filePath, $currentextension;

    public function __construct($upload_folder, $upload_path, array $extensions){
        $this->upload_folder = $upload_folder;
        $this->upload_path = $upload_path;
        $this->extensions = $extensions;
    }

    public function isValidFile(UploadedFile $file=null,$size) {
        if (!$file){
            return false;
        }
        $this->currentextension = strtolower($file->guessExtension());
        $file_size = $file->getClientSize();
        $valide_size = $file_size < $size;
        if (in_array($this->currentextension, $this->extensions) && $valide_size){
            return true;
        }
        return false;
    }

    /**
     * @param UploadedFile $file
     * @param $filename
     * @return $this
     */
    public function upload(UploadedFile $file=null, $filename){
        $fileName = $filename . '.' . $this->currentextension;
        $file->move($this->upload_path, $fileName);
        return $this;
    }

    /**
     * @param Request $request
     * @param string $fileName
     * @return $this
     */
    public function setFilePath(string $fileName){
        $this->filePath =  $this->upload_folder . '/' . $fileName . "." . $this->currentextension;
        return $this;
    }

    public function getFilePath(){
        return $this->filePath;
    }

}