<?php
/**
 * Created by PhpStorm.
 * User: ndalaba
 * Date: 04/04/2017
 * Time: 20:23
 */

namespace App\Utils;

use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Str {
    private $encoder;
    private const ENCRYPT_METHOD = "AES-256-CBC";
    private const SECRET_KEY = "AZERTY";
    private const SECRET_IV = "PROTO";
    public const ENCRYPT = "encrypt";
    public const DECRYPT = "decrypt";

    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
    }

    public function str_random($length = 50) {
        $alphabet = "0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN";
        return substr(str_shuffle(str_repeat($alphabet, $length)), 0, $length);
    }

    public function slug($string) {
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
    }

    public function encodePassword(User $user) {
        if (!$user->getPlainPassword()) {
            return;
        }
        $encoded = $this->encoder->encodePassword($user, $user->getPlainPassword());
        return $encoded;
    }

    public function encrypt_decrypt($string, $action = self::ENCRYPT) {
        $output = false;
        $key = hash('sha256', self::SECRET_KEY);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', self::SECRET_IV), 0, 16);
        if ($action===self::ENCRYPT){
            $output = openssl_encrypt($string, self::ENCRYPT_METHOD, $key, 0, $iv);
            $output = base64_encode($output);
        }
        elseif ($action===self::DECRYPT)
            $output = openssl_decrypt(base64_decode($string), self::ENCRYPT_METHOD, $key, 0, $iv);
        return $output;
    }
}