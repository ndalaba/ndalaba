<?php
/**
 * Created by PhpStorm.
 * User: ndalaba
 * Date: 04/04/2017
 * Time: 20:23
 */

namespace App\Utils;

class Moment
{
    /**
     * @param \DateTime $dateTime
     * @return string
     */
    public function timeAgo(\DateTime $dateTime)
    {

        $timestamps = $dateTime->getTimestamp();

        $secondes = time() - $timestamps;

        $secondes_string = ($secondes > 1) ? ($secondes . ' secondes') : ($secondes . ' seconde');
        $minutes = $heures = $jours = 0;
        $minutes_string = '';
        $heures_string = '';
        $jours_string = '';

        $dateRelative = 'Il y a ' . $secondes_string;
        if ($secondes > 0) {
            $left = 'Il ya ';
        } else {
            $left = 'Dans ';
        }

        // Début du balayage
        $secondes = abs($secondes);

        if ($secondes > 60) { // S'il y a plus d'une minute
            $minutes = floor($secondes / 60);
            $minutes_string = ($minutes > 1) ? $minutes . ' minutes' : $minutes . ' minute';
            $secondes = floor($secondes % 60);
            $secondes_string = ($secondes > 1) ? $secondes . ' secondes' : $secondes . ' seconde';
            $dateRelative = $left . $minutes_string . ' et ' . $secondes_string;
        }

        if ($minutes > 60) { // S'il y a plus d'une heure
            $heures = floor($minutes / 60);
            $heures_string = $heures > 1 ? $heures . ' heures' : $heures . ' heure';
            $minutes = floor($minutes % 60);
            $minutes_string = $minutes > 1 ? $minutes . ' minutes' : $minutes . ' minute';
            $dateRelative = $left . $heures_string . ' et ' . $minutes_string;
        }

        if ($heures > 24) { // S'il y a plus d'un jour

            $jours = floor($heures / 24);
            $jours_string = $jours > 1 ? $jours . ' jours' : $jours . ' jour';
            $heures = floor($heures % 24);
            $heures_string = $heures > 1 ? $heures . ' heures' : $heures . ' heure';

            $dateRelative = $left . $jours_string . ' et ' . $heures_string;
        }

        if ($jours > 7) { // S'il y a plus d'une semaine, on affiche la date normale

            $mois = date("m", $timestamps) - 1;
            $calendrier = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');

            $jour = date("j", $timestamps);
            $mois = $calendrier[$mois];
            date("Y", $timestamps) != date("Y") ? $annee = date("Y", $timestamps) : $annee = '';
            $heures = date('H:i', $timestamps);;

            $dateRelative = 'Le ' . $jour . ' ' . $mois . ' ' . $annee . ' à ' . $heures;
        }

        return $dateRelative;
    }

    function getRelativeTime($date)
    {
        $date_a_comparer = new \DateTime($date);
        $date_actuelle = new \DateTime("now");

        $intervalle = $date_a_comparer->diff($date_actuelle);

        if ($date_a_comparer > $date_actuelle) {
            $prefixe = 'dans ';
        } else {
            $prefixe = 'il y a ';
        }

        $ans = $intervalle->format('%y');
        $mois = $intervalle->format('%m');
        $jours = $intervalle->format('%d');
        $heures = $intervalle->format('%h');
        $minutes = $intervalle->format('%i');
        $secondes = $intervalle->format('%s');

        if ($ans != 0) {
            $relative_date = $prefixe . $ans . ' an' . (($ans > 1) ? 's' : '');
            if ($mois >= 6) {
                $relative_date .= ' et demi';
            }
        } elseif ($mois != 0) {
            $relative_date = $prefixe . $mois . ' mois';
            if ($jours >= 15) {
                $relative_date .= ' et demi';
            }
        } elseif ($jours != 0) {
            $relative_date = $prefixe . $jours . ' jour' . (($jours > 1) ? 's' : '');
        } elseif ($heures != 0) {
            $relative_date = $prefixe . $heures . ' heure' . (($heures > 1) ? 's' : '');
        } elseif ($minutes != 0) {
            $relative_date = $prefixe . $minutes . ' minute' . (($minutes > 1) ? 's' : '');
        } else {
            $relative_date = $prefixe . ' quelques secondes';
        }

        return $relative_date;
    }

    function rdatetime_en($dateTime, $ref = 0)
    {
        $timestamp = $dateTime->getTimestamp();

        if ($ref < 1) {
            $ref = time();
        }

        $ts = $ref - $timestamp;
        $past = $ts > 0;
        $ts = abs($ts);

        if ($past) {
            $left = 'Il ya ';
            $right = '';
        } else {
            $left = 'Dans ';
            $right = '';
        }

        if ($ts === 0) {
            return 'Maintenant';
        }

        if ($ts === 1) {
            return $left . '1 seconde' . $right;
        }

        // Less than 1 minute
        if ($ts < 60) {
            return $left . $ts . ' secondes' . $right;
        }

        $tm = floor($ts / 60);
        $ts = $ts - $tm * 60;

        // Less than 3 hours
        if ($tm < 3 && $ts > 0) {
            return $left . $tm . ' minute' . ($tm > 1 ? 's' : '') . ' et ' . $ts . ' second' . ($ts > 1 ? 's' : '') . $right;
        }

        // Less than 1 hour
        if ($tm < 60) {
            if ($ts > 0) {
                $left = 'Environ ';
            }
            return $left . $tm . ' minutes' . $right;
        }

        $th = floor($tm / 60);
        $tm = $tm - $th * 60;

        // Less than 3 hours
        if ($th < 3) {
            if ($tm > 0) {
                return $left . $th . ' heure' . ($th > 1 ? 's' : '') . ' et ' . $tm . ' minute' . ($tm > 1 ? 's' : '') . $right;
            } else {
                return $left . $th . ' heure' . ($th > 1 ? 's' : '') . $right;
            }
        }

        $refday = strtotime(date('Y-m-d', $ref));
        $refyday = strtotime(date('Y-m-d', $ref - 86400));

        // Same day, or yesterday
        if ($timestamp >= $refyday) {
            if ($timestamp < $refday) {
                $left = 'Hier ';
                $right = '';
            } else {
                if (($timestamp - $refday) == 0) {
                    $left = "Aujourd'hui";
                    return $left . ' à ' . date('H:i', $timestamp);
                }
                if (($timestamp - $refday) == 1) {
                    $left = "Demain";
                    return $left . ' à ' . date('H:i', $timestamp) . ' ' . $right;
                } else {
                    $mois = date("m", $timestamp) - 1;
                    $calendrier = array('jan', 'fév', 'mars', 'avril', 'mai', 'juin', 'juill', 'août', 'sept', 'oct', 'nov', 'déc');
                    $jours = array('lun', 'mar', 'mer', 'jeu', 'ven', 'sam', 'dim');
                    $jour = date("N", $timestamp) - 1;
                    $mois = $calendrier[$mois];
                    $jour = $jours[$jour];
                    return 'Le ' . $jour . ' ' . date("d", $timestamp) . ' ' . $mois . ' ' . date("Y", $timestamp);
                }
            }
        }

        $td = floor($th / 24);
        $th = $th - $td * 24;

        // Less than 3 days
        if ($td < 3) {
            $left = 'Derniers ';
            $right = '';
            return $left . strtolower(date('l', $timestamp)) . ' à ' . date('H:i', $timestamp) . $right;
        }

        // Less than 5 days
        if ($td < 5) {
            return $left . $td . ' jours' . $right;
        }

        $refday = strtotime(date('Y-m-01', $ref));

        $right = '';

        // Same month
        if ($timestamp >= $refyday) {

            $mois = date("m", $timestamp) - 1;
            $calendrier = array('jan', 'fév', 'mars', 'avril', 'mai', 'juin', 'juill', 'août', 'sept', 'oct', 'nov', 'déc');
            $jours = array('lun', 'mar', 'mer', 'jeu', 'ven', 'sam', 'dim');
            $jour = date("N", $timestamp) - 1;
            $mois = $calendrier[$mois];
            $jour = $jours[$jour];
            return 'Le ' . $jour . ' ' . date("d", $timestamp) . ' ' . $mois . ' ' . date("Y", $timestamp);
        }

        return date('F j, Y', $timestamp) . $right;
    }
}
