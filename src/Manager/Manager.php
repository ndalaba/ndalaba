<?php

namespace App\Manager;


use Doctrine\ORM\EntityManagerInterface;


abstract class Manager {
    /** @var EntityManagerInterface */
    protected $entityManager;
    /** @var \Doctrine\Common\Persistence\ObjectRepository */
    protected $repository;

    public function __construct(EntityManagerInterface $entityManager, string $entity) {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository($entity);
    }

    public function get(int $id) {
        return $this->repository->find($id);
    }

    public function flush($entity) {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        return $entity;
    }

    public function remove($entity) {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }

    public function oneBy(array $critere) {
        return $this->repository->findOneBy($critere);
    }

    public function findOneBySlug($slug) {
        $array = explode("-", $slug);
        $id = end($array);
        return $this->repository->findOneBy(['uid' => $id]);
    }

}