<?php

namespace App\Manager;

use App\Entity\Preference;
use App\Entity\Proposition;
use App\Entity\Trajet;
use App\Entity\User;
use App\Entity\Annonce;
use App\Utils\EmailManager;
use App\Utils\FacebookUser;
use App\Utils\Str;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserManager extends Manager {

    private $str;

    public function __construct(EntityManagerInterface $entityManager, Str $str) {
        parent::__construct($entityManager, User::class);
        $this->str = $str;
    }

    public function create(User $user) {
        $user->setRoles(array('ROLE_USER'));
        $user->setPassword($this->str->encodePassword($user));
        $this->flush($user);
        return $user;
    }


}