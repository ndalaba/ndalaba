<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Work
 *
 * @ORM\Table(name="works")
 * @ORM\Entity(repositoryClass="App\Repository\WorkRepository")
 */
class Work extends Base {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="techno", type="string", length=255, nullable=true)
     */
    private $techno;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="works")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    public function getId(): int {
        return $this->id;
    }

    public function setTitle(string $title): Work {
        $this->title = $title;

        return $this;
    }

    public function getTitle(): string {
        return $this->title;
    }

    public function setDescription(string $description): Work {
        $this->description = $description;

        return $this;
    }

    public function getDescription(): string {
        return $this->description;
    }

    public function setTechno(string $techno): Work {
        $this->techno = $techno;

        return $this;
    }

    public function getTechno(): string {
        return $this->techno;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Work
     */
    public function setUser(User $user) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser() {
        return $this->user;
    }
}
