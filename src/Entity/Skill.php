<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Skill
 *
 * @ORM\Table(name="skills")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SkillRepository")
 */
class Skill extends Base {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="skill", type="string", length=255)
     */
    private $skill;

    /**
     * @var string
     *
     * @ORM\Column(name="level", type="string", length=255)
     */
    private $level;

    /**
     * @var int
     *
     * @ORM\Column(name="experience", type="integer")
     */
    private $experience;

    /**
     * @var int
     *
     * @ORM\Column(name="percent", type="integer")
     */
    private $percent;

    /**
     * @var string
     *
     * @ORM\Column(name="skills", type="text", nullable=true)
     */
    private $skills;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="skills")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    public function getId(): int {
        return $this->id;
    }

    public function setSkill(string $skill): Skill {
        $this->skill = $skill;

        return $this;
    }

    public function getSkill(): string {
        return $this->skill;
    }

    public function setLevel(string $level): Skill {
        $this->level = $level;

        return $this;
    }

    public function getLevel(): string {
        return $this->level;
    }

    public function setExperience(int $experience): Skill {
        $this->experience = $experience;

        return $this;
    }

    public function getExperience(): int {
        return $this->experience;
    }

    public function setPercent(int $percent): Skill {
        $this->percent = $percent;

        return $this;
    }

    public function getPercent(): int {
        return $this->percent;
    }

    public function setSkills(string $skills): Skill {
        $this->skills = $skills;

        return $this;
    }

    public function getSkills(): string {
        return $this->skills;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Skill
     */
    public function setUser(User $user) {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser() {
        return $this->user;
    }
}
