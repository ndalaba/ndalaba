<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use \Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="Email déja enregistré")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends Base implements UserInterface, EquatableInterface{
    const PHOTO_SIZE = 1000000;
    const PHOTO_SIZE_LABEL = "La taille de l'image doit être inférieur à 1M";

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="job", type="string", length=255,nullable=true)
     */
    private $job;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255,nullable=true)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="json_array")
     */
    private $roles = [];

    /**
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    private $plainPassword;

    /**
     * @ORM\Column(name="lastLogin", type="datetime",nullable=true)
     */
    private $lastLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, unique=true,nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, unique=true,nullable=true)
     * @Assert\Image(mimeTypes = {"image/jpeg", "image/gif", "image/png"})
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="bio", type="text", nullable=true)
     */
    private $bio;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=255, nullable=true, unique=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", length=255, nullable=true, unique=true)
     */
    private $twitter;

    /**
     * @var string
     *
     * @ORM\Column(name="googleplus", type="string", length=255, nullable=true, unique=true)
     */
    private $googleplus;

    /**
     * @var string
     *
     * @ORM\Column(name="youtube", type="string", length=255, nullable=true, unique=true)
     */
    private $youtube;

    /**
     * @var string
     *
     * @ORM\Column(name="linkedin", type="string", length=255, nullable=true, unique=true)
     */
    private $linkedin;

    /**
     * @ORM\OneToMany(targetEntity="Education", mappedBy="user",cascade={"remove"})
     */
    private $educations;

    /**
     * @ORM\OneToMany(targetEntity="Job", mappedBy="user",cascade={"remove"})
     */
    private $jobs;

    /**
     * @ORM\OneToMany(targetEntity="Service", mappedBy="user",cascade={"remove"})
     */
    private $services;

    /**
     * @ORM\OneToMany(targetEntity="Skill", mappedBy="user",cascade={"remove"})
     */
    private $skills;

    /**
     * @ORM\OneToMany(targetEntity="Work", mappedBy="user",cascade={"remove"})
     */
    private $works;

    public function __construct() {
        $this->setUid();
    }

    /**
     * @return mixed
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name){
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @param string $job
     * @return $this
     */
    public function setJob(string $job){
        $this->job = $job;

        return $this;
    }

    /**
     * @return string
     */
    public function getJob(){
        return $this->job;
    }

    /**
     * @param string $location
     * @return $this
     */
    public function setLocation(string $location){
        $this->location = $location;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocation(){
        return $this->location;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email){
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(){
        return $this->email;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function setPhone(string $phone){
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(){
        return $this->phone;
    }

    /**
     * @param string $bio
     * @return $this
     */
    public function setBio(string $bio){
        $this->bio = $bio;

        return $this;
    }

    /**
     * @return string
     */
    public function getBio(){
        return $this->bio;
    }

    /**
     * @param string $facebook
     * @return $this
     */
    public function setFacebook(string $facebook){
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * @return string
     */
    public function getFacebook(){
        return $this->facebook;
    }

    /**
     * @param string $twitter
     * @return $this
     */
    public function setTwitter(string $twitter){
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * @return string
     */
    public function getTwitter(){
        return $this->twitter;
    }

    /**
     * @param string $googleplus
     * @return $this
     */
    public function setGoogleplus(string $googleplus){
        $this->googleplus = $googleplus;

        return $this;
    }

    /**
     * @return string
     */
    public function getGoogleplus(){
        return $this->googleplus;
    }

    /**
     * @param string $youtube
     * @return $this
     */
    public function setYoutube(string $youtube){
        $this->youtube = $youtube;

        return $this;
    }

    /**
     * @return string
     */
    public function getYoutube(){
        return $this->youtube;
    }

    /**
     * @param string $linkedin
     * @return $this
     */
    public function setLinkedin(string $linkedin){
        $this->linkedin = $linkedin;

        return $this;
    }

    /**
     * @return string
     */
    public function getLinkedin(){
        return $this->linkedin;
    }

    /**
     * Get educations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEducations(){
        return $this->educations;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return User
     */
    public function setPhoto($photo){
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto(){
        return $this->photo;
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobs(){
        return $this->jobs;
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices(){
        return $this->services;
    }

    /**
     * Get skills
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSkills(){
        return $this->skills;
    }

    /**
     * Get works
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorks(){
        return $this->works;
    }

    /**
     * @return mixed
     */
    public function getPassword(){
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword(string $password){
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlainPassword(){
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     * @return $this
     */
    public function setPlainPassword(string $plainPassword){
        $this->plainPassword = $plainPassword;
        return $this;
    }


    public function getRoles(){
        return $this->roles;
    }
    /**
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles){
        if (!in_array('ROLE_USER', $roles)){
            $roles[] = 'ROLE_USER';
        }
        $this->roles = $roles;
        return $this;
    }

    public function getSalt(){
    }

    public function eraseCredentials(){
        $this->password = null;
    }

    /**
     * @return string
     */
    public function getUsername(){
        return $this->email;
    }

    /**
     * Set lastLogin
     *
     * @param \DateTime $lastLogin
     *
     * @return User
     */
    public function setLastLogin(\DateTime $lastLogin){
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Get lastLogin
     *
     * @return \DateTime
     */
    public function getLastLogin(){
        return $this->lastLogin;
    }

    /**
     * @param UserInterface $user
     * @return bool
     */
    public function isEqualTo(UserInterface $user){
        return $this->id === $user->getId();
    }

}
