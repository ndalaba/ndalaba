<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Job
 *
 * @ORM\Table(name="jobs")
 * @ORM\Entity(repositoryClass="App\Repository\JobRepository")
 */
class Job extends Base {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255)
     */
    private $company;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin", type="datetime")
     */
    private $begin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime")
     */
    private $end;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="jobs")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    public function getId(): int {
        return $this->id;
    }

    public function setTitle(string $title): Job {
        $this->title = $title;

        return $this;
    }

    public function getTitle(): string {
        return $this->title;
    }

    public function setLocation(string $location): Job {
        $this->location = $location;

        return $this;
    }

    public function getLocation(): string {
        return $this->location;
    }

    public function setCompany(string $company) {
        $this->company = $company;

        return $this;
    }

    public function getCompany(): string {
        return $this->company;
    }

    public function setBegin(\DateTime $begin): Job {
        $this->begin = $begin;

        return $this;
    }

    public function getBegin(): \Datetime {
        return $this->begin;
    }

    public function setEnd(\Datetime $end): Job {
        $this->end = $end;

        return $this;
    }

    public function getEnd(): \Datetime {
        return $this->end;
    }

    public function setDescription(string $description): Job {
        $this->description = $description;

        return $this;
    }

    public function getDescription(): string {
        return $this->description;
    }

    public function setUser(User $user) {
        $this->user = $user;

        return $this;
    }

    public function getUser(): User {
        return $this->user;
    }
}
