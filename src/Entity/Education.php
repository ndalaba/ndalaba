<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Education
 *
 * @ORM\Table(name="educations")
 * @ORM\Entity(repositoryClass="App\Repository\EducationRepository")
 */
class Education extends Base {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="formation", type="string", length=255)
     */
    private $formation;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="school", type="string", length=255)
     */
    private $school;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin", type="datetime")
     */
    private $begin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime")
     */
    private $end;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="educations")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    public function getId(): int {
        return $this->id;
    }

    public function setFormation(string $formation): Education {
        $this->formation = $formation;

        return $this;
    }

    public function getFormation(): string {
        return $this->formation;
    }

    public function setLocation(string $location): Education {
        $this->location = $location;

        return $this;
    }

    public function getLocation(): string {
        return $this->location;
    }

    public function setSchool($school) {
        $this->school = $school;

        return $this;
    }

    public function getSchool(): string {
        return $this->school;
    }

    public function setBegin(\DateTime $begin): Education {
        $this->begin = $begin;

        return $this;
    }

    public function getBegin(): \Datetime {
        return $this->begin;
    }

    public function setEnd(\Datetime $end): Education {
        $this->end = $end;

        return $this;
    }

    public function getEnd(): \Datetime {
        return $this->end;
    }

    public function setDescription(string $description): Education {
        $this->description = $description;

        return $this;
    }

    public function getDescription(): string {
        return $this->description;
    }

    public function setUser(User $user) {
        $this->user = $user;

        return $this;
    }

    public function getUser(): User {
        return $this->user;
    }
}
