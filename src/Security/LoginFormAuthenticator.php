<?php

namespace App\Security;


use App\Manager\UserManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

use App\Form\LoginFormType;
use Symfony\Component\Security\Http\Util\TargetPathTrait;


class LoginFormAuthenticator extends AbstractGuardAuthenticator {

    private $formFactory, $userManager, $router, $passwordEncoder, $remember, $session;

    public function __construct(FormFactoryInterface $formFactory, UserManager $userManager, RouterInterface $router, UserPasswordEncoderInterface $encoder) {
        $this->formFactory = $formFactory;
        $this->userManager = $userManager;
        $this->router = $router;
        $this->passwordEncoder = $encoder;
        $this->session = new Session();

    }

    public function supports(Request $request) {
        return $request->getPathInfo() == '/connexion' && $request->isMethod('POST');
    }

    public function getCredentials(Request $request) {
        $form = $this->formFactory->create(LoginFormType::class);
        $form->handleRequest($request);
        $data = $form->getData();
        $this->remember = $request->request->get('_remember_me');
        $request->getSession()
            ->set(Security::LAST_USERNAME, $data['_username']);
        return $data;
    }

    public function getUser($credentials, UserProviderInterface $userProvider) {
        $username = $credentials['_username'];
        $user = $this->userManager->byEmail($username);
        if (!$user) {
            $this->session->getFlashBag()
                ->set('error', "Votre e-mail ou votre mot de passe est invalide. Réessayez.");
        }
        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user) {
        $password = $credentials['_password'];
        if ($this->passwordEncoder->isPasswordValid($user, $password)) {
            $user->setLastLogin(new \DateTime());
            $this->userManager->flush($user);
            return true;
        }
        $this->session->getFlashBag()
            ->set('error', "Votre e-mail ou votre mot de passe est invalide. Réessayez.");
        return false;
    }

    public function start(Request $request, AuthenticationException $authException = null) {
        $url = $this->router->generate('login');
        return new RedirectResponse($url);
    }

    use TargetPathTrait;

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey) {
        $targetPath = $this->getTargetPath($request->getSession(), $providerKey);
        if (!$targetPath) {
            return new RedirectResponse($this->router->generate('compte'));
        }

        return new RedirectResponse($targetPath);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception) {
        $request->getSession()
            ->set(Security::AUTHENTICATION_ERROR, $exception);
        return new RedirectResponse($this->router->generate('login'));
    }

    public function supportsRememberMe() {
        return $this->remember;
    }

}
