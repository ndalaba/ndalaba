<?php

namespace App\Controller\Security;

use App\Entity\User;
use App\Form\ResetPasswordType;
use App\Utils\EmailManager;
use App\Manager\UserManager;
use App\Utils\Str;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Form\LoginFormType;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class SecurityController extends AbstractController {


    /**
     * @Route("reset/{id}/{confirmation_token}",name="reset_password")
     * @param Request $request
     * @param User $user
     * @param $confirmation_token
     * @param UserManager $userManager
     * @param Str $str
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function resetAction(Request $request, User $user = null, $confirmation_token, UserManager $userManager, Str $str) {

        if ($user && $user->getConfirmationToken() === $confirmation_token) {
            $title = "Modifier mon mot de passe";
            $form = $this->createForm(ResetPasswordType::class);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $user->setPlainPassword($form->get('plainPassword')->getData());
                $encoded = $str->encodePassword($user);
                $user->setConfirmationToken($str->str_random());
                $user->setPassword($encoded);

                $userManager->flush($user);
                return $this->redirectToRoute('login', ['success' => "Votre mot de passe a été modifiée"]);
            }
            return $this->render(':security:reset_password.html.twig', ['title' => $title, 'form' => $form->createView()]);
        } else {
            $this->addFlash('error', "Ce compte n'existe pas. Veillez vous inscrire");
            return $this->redirectToRoute('register');
        }
    }


    /**
     * @Route("mot-de-passe-oublie",name="password_oublie")
     * @param Request $request
     * @param UserManager $userManager
     * @param EmailManager $emailManager
     * @param Str $str
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function passwordAction(Request $request, UserManager $userManager, EmailManager $emailManager,Str $str) {
        $error = null;
        $email = null;

        if ($request->isMethod("POST")) {
            $email = $request->request->get('ask_for_new_password');
            $user = $userManager->byEmail($email);
            if (!$user) {
                $error = "Cette adresse email incorrecte";
            } else {
                $user->setConfirmationToken($str->str_random());
                $this->getDoctrine()
                    ->getManager()
                    ->flush();
                $renderEmail = $this->renderView(':email:ask_reset_email.html.twig', array('user' => $user));
                $status = $emailManager->sendMail(Colis::EMAIL_FROM, $user->getEmail(), 'Validation de votre Compte', $renderEmail);
                $this->addFlash('success', "Consulter votre mail pour la réinitialisation de votre mot de passe ");
                return $this->redirectToRoute('login');
            }
        }
        return $this->render('security/mot_de_passe_oublie.html.twig', array('title' => "Mot de passe oublié", 'error' => $error, 'email' => $email));
    }

    /**
     * @Route("connexion",name="login")
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils) {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        $form = $this->createForm(LoginFormType::class, ['_username' => $lastUsername]);
        $title = "Connexion à mon compte";
        $success = $request->query->get('success', '');// si redirection venant de la modification de mot de passe
        return $this->render('security/login.html.twig', array(//  'last_username' => $lastUsername,
            'form' => $form->createView(), 'error' => $error, 'title' => $title, 'success' => $success));
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction() {
        throw new \Exception('this should not be reached!');
    }
}
