<?php

namespace App\Controller;

use App\Form\UserEditType;
use App\Utils\FileUploader;
use App\Manager\UserManager;
use App\Utils\Str;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class ProfilController extends Controller {
    /**
     * @Route("compte", name="compte")
     * @param Str $str
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index() {
        return $this->render('front/home.twig', array('title' => "Tableau de bord"));
    }

    /**
     * @Route("compte/modifier-mot-de-passe",name="password_edit")
     * @param Request $request
     * @param UserManager $userManager
     * @param Str $str
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function password(Request $request, UserManager $userManager, Str $str) {
        $title = "Modifier mon mot de passe";
        $user = $this->getUser();
        $form = $this->createForm(ResetPasswordType::class, null);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPlainPassword($form->get("plainPassword")->getData());
            $encoded = $str->encodePassword($user);
            $user->setPassword($encoded);
            $userManager->flush($user);
            $this->addFlash('success', "Votre mot de passe a été modifiée");
            return $this->redirectToRoute('compte');
        }

        return $this->render('security/reset_password.html.twig', ['title' => $title, 'form' => $form->createView()]);
    }

    /**
     * @Route("compte/modifier",name="user_edit")
     * @param Request $request
     * @param UserManager $userManager
     * @param FileUploader $uploader
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function edit(Request $request, UserManager $userManager, FileUploader $uploader) {
        $title = "Modifier mon compte";
        $user = $this->getUser();
        $form = $this->createForm(UserEditType::class, $user);
        $form_password = $this->createForm(ResetPasswordType::class, null, array('action' => $this->generateUrl('password_edit')));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($uploader->isValidFile($user->getPhoto(), User::PHOTO_SIZE)) {
                $filename = $user->getId() . "_" . $user->getFirstname();
                $filePath = $uploader->setFilePath($request, $filename)
                    ->upload($user->getPhoto(), $filename)
                    ->getFilePath();
                $user->setPhoto($filePath);
            } else {
                $user->setPhoto($request->request->get('last_photo'));
            }
            $hidePhone = ($request->request->get('hide_phone') !== null) ? 1 : 0;
            if ($user->getPreference())
                $user->getPreference()->setHidePhone($hidePhone);
            $userManager->flush($user);
            $this->addFlash('success', "Votre compte a été modifié");
            return $this->redirectToRoute('user_edit');
        }
        return $this->render('front/membre/edit.html.twig', ['title' => $title, 'form' => $form->createView(), 'form_password' => $form_password->createView()]);
    }

    /**
     * @Route("compte/preference",name="set_preference")
     * @param Request $request
     * @param UserManager $userManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function preference(Request $request, UserManager $userManager) {
        $title = "Modifier mon compte";
        $user = $this->getUser();
        if ($request->isMethod('POST')) {
            $hidePhone = ($request->request->get('hide_phone') !== null) ? 1 : 0;
            $user->getPreference()->setHidePhone($hidePhone);
            $userManager->flush($user);
            $this->addFlash('success', "Vos préférences ont été modifiées");
            return $this->redirectToRoute('compte');
        } else
            return $this->render('front/membre/preference.html.twig', ['title' => $title]);
    }

}