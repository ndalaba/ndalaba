<?php
/**
 * Created by PhpStorm.
 * User: ndalaba
 * Date: 28/03/2017
 * Time: 09:31
 */

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository{

    public function byEmail($email){
        return $this->createQueryBuilder("u")
                    ->where('u.email=:email')
                    ->setParameter("email", $email)
                    ->getQuery()
                    ->getSingleResult();
    }

}