<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('gender', ChoiceType::class,
            array('label' => false,
                'attr' => array('placeholder' => "", 'class' => 'gender'),
                'choices' => array(
                    'Homme' => "male",
                    'Femme' => "female"
                ),
                'expanded' => true,
                'empty_data' => null
            ))
            ->add('firstname', TextType::class, array('label' => false, 'attr' => array('placeholder' => "Prénom")))
            ->add('lastname', TextType::class, array('label' => false, 'attr' => array('placeholder' => "Nom")))
            ->add('email', EmailType::class, array('label' => false, 'attr' => array('placeholder' => "Email")))
            ->add('phone', TextType::class, array('label' => false, 'attr' => array('placeholder' => "Téléphone")))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe doivent être identiques.',
                'first_options' => array('label' => false, 'attr' => array('placeholder' => "Mot de passe")),
                'second_options' => array('label' => false, 'attr' => array('placeholder' => "Confirmer le mot de passe")),
            ))
            ->add('naissance', DateType::class, ["label" => "Année de naissance ", 'widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr' => ['class' => 'js-datepicker'], "html5" => false]);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'App\Entity\User'));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_user';
    }
}
