<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEditType extends AbstractType {
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('gender', ChoiceType::class,
                array('label' => "Sexe",
                    'attr' => array('placeholder' => "", 'class' => 'gender gender-edit'),
                    'choices' => array(
                        'Homme' => "male",
                        'Femme' => "female"
                    ),
                    'expanded' => true,
                    'empty_data' => null
                ))
            ->add('firstname', TextType::class, array('label' => "Prénom", 'attr' => array('placeholder' => "Prénom")))
            ->add('lastname', TextType::class, array('label' => "Nom", 'attr' => array('placeholder' => "Nom")))
            ->add('email', EmailType::class, array('label' => "Email", 'attr' => array('placeholder' => "Email")))
            ->add('phone', TextType::class, array('label' => "Télephone", 'attr' => array('placeholder' => "Téléphone")))
            ->add('naissance', DateType::class, ["label" => "Année de naissance ", 'widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr' => ['class' => 'js-datepicker'], "html5" => false])
            /*->add('naissance', ChoiceType::class,
                array('label' => "Année de naissance",
                    'attr' => array('placeholder' => "Naissance"),
                    'choices' => User::getNaissanceField()
                )
            )*/
            ->add('adresse', TextType::class, array('label' => "Adresse", "required" => false, 'attr' => array('placeholder' => "Votre adresse complète")))
            ->add('description', TextareaType::class, array('label' => "Description", "required" => false, 'attr' => array('placeholder' => "Présentez-vous aux autres membres")))
            ->add('photo', FileType::class, array('label' => "Ajouter votre photo", 'data_class' => null, "required" => false));

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array('data_class' => 'App\Entity\User'));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'app_user';
    }
}
