<?php

namespace App\Twig;

use App\Utils\Moment;
use App\Utils\Str;

class AppExtension extends \Twig_Extension {

    private $str, $moment;

    public function __construct(Str $str, Moment $moment) {
        $this->str = $str;
        $this->moment = $moment;
    }

    public function getFilters() {
        return [new \Twig_SimpleFilter('timeago', [$this, 'timeAgo']), new \Twig_SimpleFilter('slugify', [$this, 'slugify'])];
    }

    /**
     * @param \DateTime $datetime
     * @return string
     */
    public function timeAgo($datetime) {
        return $this->moment->timeAgo($datetime);
    }

    /**
     * @param $string
     * @return string
     */
    public function slugify($string) {
        return $this->str->slug($string);
    }
}